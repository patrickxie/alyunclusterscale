package common

import (
	"fmt"
	"time"
)

type YunMachine struct {
	Name         string
	Id           string
	HostName     string
	PublicIP     string
	InnerIP      string
	ImageId      string
	CreationTime time.Time
	ExpiredTime  time.Time
	Status       MachineStatus
	CPU          int
	Memory       int
	Description  string
}

type MachineStatus string

const (
	MachineRunning  MachineStatus = "Running"
	MachineStopped  MachineStatus = "Stopped"
	MachineStarting MachineStatus = "Starting"
	MachineStopping MachineStatus = "Stopping"
)

type YunProvider interface {
	GetAllMachines() ([]YunMachine, error)
	GetRunningMachines() ([]YunMachine, error)
	GetMachineByName(name string) (YunMachine, error)
	GetMachineById(id string) (YunMachine, error)
	GetMachineByPubliceIP(ip string) (YunMachine, error)
	CreateMachine(opsFunc func() interface{}) (YunMachine, error)
	CreateMachineWait(opsFunc func() interface{}, timeOut int) (YunMachine, error)
	RestartMachine(machine *YunMachine, force bool) error
	RestartMachineWait(machine *YunMachine, force bool, timeOut int) error
	StopMachine(machine *YunMachine, force bool) error
	StopMachineWait(machine *YunMachine, force bool, timeOut int) error
	StartMachine(machine *YunMachine) error
	StartMachineWait(machine *YunMachine, timeOut int) error
	DeleteMachine(machine *YunMachine) error
	ModifyMachine(machine *YunMachine, toMachine *YunMachine) error
	SetCacheEnable(v bool)
}

type Spec struct {
	NumCpu int
	MemGB  int
}

//https://help.aliyun.com/document_detail/25378.html?spm=5176.doc25499.2.13.frzYlT
var MachineSpec = map[string]Spec{
	/* 通用型实例规格族 g5
		   超高网络 PPS 收发包能力
	       处理器：2.5 GHz 主频的 Intel Xeon Platinum 8163（Skylake），计算性能稳定
	       实例网络性能与计算规格对应（规格越高网络性能强）
	       适用场景：
		       高网络包收发场景，如视频弹幕、电信业务转发等
	           各种类型和规模的企业级应用
	           中小型数据库系统、缓存、搜索集群
	           数据分析和计算
			   计算集群、依赖内存的数据处理
	*/
	"ecs.g5.large":    Spec{2, 4},
	"ecs.g5.xlarge":   Spec{4, 16},
	"ecs.g5.2xlarge":  Spec{8, 32},
	"ecs.g5.4xlarge":  Spec{16, 48},
	"ecs.g5.6xlarge":  Spec{24, 64},
	"ecs.g5.8xlarge":  Spec{32, 128},
	"ecs.g5.16xlarge": Spec{64, 256},

	/* 通用网络增强型实例规格族 sn2ne
	       处理器与内存配比为 1:4
		   超高网络 PPS 收发包能力
		   处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell）或 E5-2680 v3（Haswell），计算性能稳定
		   实例网络性能与计算规格对应（规格越高网络性能强）
		   适用场景：
		       高网络包收发场景，如视频弹幕、电信业务转发等
			   各种类型和规模的企业级应用
			   中小型数据库系统、缓存、搜索集群
			   数据分析和计算
			   计算集群、依赖内存的数据处理

	*/
	"ecs.sn2ne.large":    Spec{2, 8},
	"ecs.sn2ne.xlarge":   Spec{4, 16},
	"ecs.sn2ne.2xlarge":  Spec{8, 32},
	"ecs.sn2ne.4xlarge":  Spec{16, 64},
	"ecs.sn2ne.8xlarge":  Spec{32, 128},
	"ecs.sn2ne.14xlarge": Spec{56, 224},

	/* 通用型实例规格族 sn2
	       处理器与内存配比为 1:4
		   处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell）或 E5-2680 v3（Haswell），计算性能稳定
		   实例网络性能与计算规格对应（规格越高网络性能强）
		   适用场景：
		       各种类型和规模的企业级应用
			   中小型数据库系统、缓存、搜索集群
			   数据分析和计算
			   计算集群、依赖内存的数据处理

	*/
	"ecs.sn2.medium":   Spec{2, 8},
	"ecs.sn2.large":    Spec{4, 16},
	"ecs.sn2.xlarge":   Spec{8, 32},
	"ecs.sn2.3xlarge":  Spec{16, 64},
	"ecs.sn2.7xlarge":  Spec{32, 128},
	"ecs.sn2.13xlarge": Spec{56, 224},

	/* 计算型实例规格族 c5
	       处理器与内存配比为 1:2
		   超高网络 PPS 收发包能力
		   处理器：2.5 GHz 主频的 Intel Xeon Platinum 8163（Skylake），计算性能稳定
		   实例网络性能与计算规格对应（规格越高网络性能强）
		   适用场景：
		       高网络包收发场景，如视频弹幕、电信业务转发等
			   Web 前端服务器
			   大型多人在线游戏（MMO）前端
			   数据分析、批量计算、视频编码
			   高性能科学和工程应用
	*/
	"ecs.c5.large":    Spec{2, 4},
	"ecs.c5.xlarge":   Spec{4, 8},
	"ecs.c5.2xlarge":  Spec{8, 16},
	"ecs.c5.4xlarge":  Spec{16, 32},
	"ecs.c5.6xlarge":  Spec{24, 48},
	"ecs.c5.8xlarge":  Spec{32, 64},
	"ecs.c5.16xlarge": Spec{64, 128},

	/* 计算网络增强型实例规格族 sn1ne
	       处理器与内存配比为 1:2
		   超高网络 PPS 收发包能力
		   处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell）或 E5-2680 v3（Haswell），计算性能稳定
		   实例网络性能与计算规格对应（规格越高网络性能强）
		   适用场景：
		       高网络包收发场景，如视频弹幕、电信业务转发等
			   Web 前端服务器
			   大型多人在线游戏（MMO）前端
			   数据分析、批量计算、视频编码
			   高性能科学和工程应用
	*/
	"ecs.sn1ne.large":   Spec{2, 4},
	"ecs.sn1ne.xlarge":  Spec{4, 8},
	"ecs.sn1ne.2xlarge": Spec{8, 16},
	"ecs.sn1ne.4xlarge": Spec{16, 32},
	"ecs.sn1ne.8xlarge": Spec{32, 64},

	/* 计算型实例规格族 sn1
	       处理器与内存配比为 1:2
		   处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell）或 E5-2680 v3（Haswell），计算性能稳定
		   实例网络性能与计算规格对应（规格越高网络性能强）
		   适用场景：
		       Web 前端服务器
			   大型多人在线游戏（MMO）前端
			   数据分析、批量计算、视频编码
			   高性能科学和工程应用
	*/
	"ecs.sn1.medium":  Spec{2, 4},
	"ecs.sn1.large":   Spec{4, 8},
	"ecs.sn1.xlarge":  Spec{8, 16},
	"ecs.sn1.3xlarge": Spec{16, 32},
	"ecs.sn1.7xlarge": Spec{32, 64},

	/* 内存型实例规格族 r5
	       超高网络 PPS 收发包能力
		   处理器：2.5 GHz 主频的 Intel Xeon Platinum 8163（Skylake），计算性能稳定
		   实例网络性能与计算规格对应（规格越高网络性能强）
		   适用场景：
		       高网络包收发场景，如视频弹幕、电信业务转发等
			   高性能数据库、内存数据库
			   数据分析与挖掘、分布式内存缓存
			   Hadoop、Spark 群集以及其他企业大内存需求应用
	*/
	"ecs.r5.large":    Spec{2, 16},
	"ecs.r5.xlarge":   Spec{4, 32},
	"ecs.r5.2xlarge":  Spec{8, 48},
	"ecs.r5.4xlarge":  Spec{16, 64},
	"ecs.r5.6xlarge":  Spec{24, 128},
	"ecs.r5.8xlarge":  Spec{32, 256},
	"ecs.r5.16xlarge": Spec{64, 512},
	"ecs.r5.22xlarge": Spec{88, 704},

	/* 内存网络增强型实例规格族 se1ne
		       处理器与内存配比为 1:8
	           超高网络 PPS 收发包能力
			   处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell），计算性能稳定
			   实例网络性能与计算规格对应（规格越高网络性能强）
			   适用场景：
			       高网络包收发场景，如视频弹幕、电信业务转发等
				   高性能数据库、内存数据库
				   数据分析与挖掘、分布式内存缓存
				   Hadoop、Spark 群集以及其他企业大内存需求应用
	*/
	"ecs.se1ne.large":    Spec{2, 16},
	"ecs.se1ne.xlarge":   Spec{4, 32},
	"ecs.se1ne.2xlarge":  Spec{8, 64},
	"ecs.se1ne.4xlarge":  Spec{16, 128},
	"ecs.se1ne.8xlarge":  Spec{32, 256},
	"ecs.se1ne.14xlarge": Spec{56, 480},

	/* 内存型实例规格族 se1
			    处理器与内存配比为 1:8
	            处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell），计算性能稳定
				实例网络性能与计算规格对应（规格越高网络性能强）
				适用场景：
					高性能数据库、内存数据库
					数据分析与挖掘、分布式内存缓存
					Hadoop、Spark 群集以及其他企业大内存需求应用
	*/
	"ecs.se1.large":     Spec{2, 16},
	"ecs.se1.xlarge":    Spec{4, 32},
	"ecs.se1.2xlarge":   Spec{8, 64},
	"ecs.se1.4xlarge":   Spec{16, 128},
	"ecs.se1.8xlarge":   Spec{32, 256},
	"ecs.se1.14xlargee": Spec{56, 480},

	/* 大数据网络增强型实例规格族 d1ne
				    实例配备大容量、高吞吐 SATA HDD 本地盘，辅以最大 35 Gbit/s 实例间网络带宽
		            处理器与内存配比为 1:4，为大数据场景设计
					处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell）
	                实例网络性能与计算规格对应（规格越高网络性能强）
					适用场景：
						Hadoop MapReduce/HDFS/Hive/HBase 等
	                    Spark 内存计算/MLlib 等
						互联网行业、金融行业等有大数据计算与存储分析需求的行业客户，进行海量数据存储和计算的业务场景
						Elasticsearch、日志等
	*/
	"ecs.d1ne.2xlarge":  Spec{8, 32},
	"ecs.d1ne.4xlarge":  Spec{16, 64},
	"ecs.d1ne.6xlarge":  Spec{24, 96},
	"ecs.d1ne.8xlarge":  Spec{32, 128},
	"ecs.d1ne.14xlarge": Spec{56, 224},

	/* 大数据型实例规格族 d1
				    实例配备大容量、高吞吐 SATA HDD 本地盘，辅以最大 17 Gbit/s 实例间网络带宽
		            处理器与内存配比为 1:4，为大数据场景设计
					处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell）
	                实例网络性能与计算规格对应（规格越高网络性能强）
					适用场景：
						Hadoop MapReduce/HDFS/Hive/HBase 等
	                    Spark 内存计算/MLlib 等
						互联网行业、金融行业等有大数据计算与存储分析需求的行业客户，进行海量数据存储和计算的业务场景
						Elasticsearch、日志等
	*/
	"ecs.d1.2xlarge":        Spec{8, 32},
	"ecs.d1.4xlarge":        Spec{16, 64},
	"ecs.d1.6xlarge":        Spec{24, 96},
	"ecs.d1-c8d3.8xlarge":   Spec{32, 128},
	"ecs.d1.8xlarge":        Spec{56, 224},
	"ecs.d1-c14d3.14xlarge": Spec{32, 128},
	"ecs.d1.14xlarge":       Spec{56, 224},

	/* 本地 SSD 型实例规格族 i2
				    配备高性能（高 IOPS、大吞吐、低访问延迟）NVMe SSD 本地盘
		            处理器与内存配比为 1:8，为高性能数据库等场景设计
					处理器：2.5 GHz 主频的 Intel Xeon Platinum 8163（Skylake）
					实例网络性能与计算规格对应（规格越高网络性能强）
					适用场景：
						OLTP、高性能关系型数据库
	                   NoSQL 数据库（如 Cassandra、MongoDB 等）
						Elasticsearch 等搜索场景
	*/
	"ecs.i2.xlarge":   Spec{4, 32},
	"ecs.i2.2xlarge":  Spec{8, 64},
	"ecs.i2.4xlarge":  Spec{16, 128},
	"ecs.i2.8xlarge":  Spec{32, 256},
	"ecs.i2.16xlarge": Spec{64, 512},

	/* 本地 SSD 型实例规格族 i1
				    配备高性能（高 IOPS、大吞吐、低访问延迟）NVMe SSD 本地盘
					处理器与内存配比为 1:4，为高性能数据库等场景设计
					处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell）
					实例网络性能与计算规格对应（规格越高网络性能强）
					适用场景：
						OLTP、高性能关系型数据库
	                   NoSQL 数据库（如 Cassandra、MongoDB 等）
						Elasticsearch 等搜索场景
	*/
	"ecs.i1.xlarge":        Spec{4, 16},
	"ecs.i1.2xlarge":       Spec{8, 32},
	"ecs.i1.4xlarge":       Spec{16, 64},
	"ecs.i1-c5d1.4xlarge":  Spec{16, 64},
	"ecs.i1-c15d2.6xlarge": Spec{24, 96},
	"ecs.i1.8xlarge":       Spec{32, 128},
	"ecs.i1-c10d1.8xlarge": Spec{32, 128},
	"ecs.i1.14xlarge":      Spec{56, 224},

	/* 高主频计算型实例规格族 hfc5
					    计算性能稳定
						处理器：3.1 GHz 主频的 Intel Xeon Gold 6149（Skylake）
						处理器与内存配比为 1:2
						实例网络性能与计算规格对应（规格越高网络性能强）
						适用场景：
							高性能 Web 前端服务器
	    					高性能科学和工程应用
	    					MMO 游戏、视频编码
	*/
	"ecs.hfc5.large":    Spec{2, 4},
	"ecs.hfc5.xlarge":   Spec{4, 8},
	"ecs.hfc5.2xlarge":  Spec{8, 16},
	"ecs.hfc5.4xlarge":  Spec{16, 32},
	"ecs.hfc5.6xlargee": Spec{24, 48},
	"ecs.hfc5.8xlarge":  Spec{32, 64},

	/* 高主频通用型实例规格族 hfg5
				计算性能稳定
	    		处理器：3.1 GHz 主频的 Intel Xeon Gold 6149（Skylake）
	    		处理器与内存配比为 1:4（56 vCPU 规格除外）
	   			实例网络性能与计算规格对应（规格越高网络性能强）
	   			适用场景：
	        		高性能 Web 前端服务器
	        		高性能科学和工程应用
	        		MMO 游戏、视频编码

	*/
	"ecs.hfg5.large":    Spec{2, 8},
	"ecs.hfg5.xlarge":   Spec{4, 16},
	"ecs.hfg5.2xlarge":  Spec{8, 32},
	"ecs.hfg5.4xlarge":  Spec{16, 64},
	"ecs.hfg5.6xlarge":  Spec{24, 96},
	"ecs.hfg5.8xlarge":  Spec{32, 128},
	"ecs.hfg5.14xlarge": Spec{56, 160},

	/* 高主频计算型实例规格族 c4
				计算性能稳定
	    		处理器：3.2 GHz 主频的 Intel Xeon E5-2667 v4（Broadwell）处理器
	    		实例网络性能与计算规格对应（规格越高网络性能强）
	    		适用场景：
	     	        高性能 Web 前端服务器
	       	 	    高性能科学和工程应用
	        		MMO 游戏、视频编码

	*/
	"ecs.c4.xlarge":   Spec{4, 8},
	"ecs.c4.2xlarge":  Spec{8, 16},
	"ecs.c4.4xlarge":  Spec{16, 32},
	"ecs.cm4.xlarge":  Spec{4, 16},
	"ecs.cm4.2xlarge": Spec{8, 32},
	"ecs.cm4.4xlarge": Spec{16, 64},
	"ecs.cm4.6xlarge": Spec{24, 96},
	"ecs.ce4.xlarge":  Spec{4, 32},

	/* GPU 计算型实例规格族 gn5
	 			采用 NVIDIA P100 GPU 计算卡
		   		多种 CPU 和 Memory 配比
		   		高性能 NVMe SSD 数据缓存盘
		   		处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell）
		   		实例网络性能与计算规格对应（规格越高网络性能强）
		   		适用场景：
		       		深度学习
		       		科学计算，如计算流体动力学、计算金融学、基因组学研究、环境分析
		       		高性能计算、渲染、多媒体编解码及其他服务器端 GPU 计算工作负载
	*/
	"ecs.gn5-c4g1.xlarge":    Spec{4, 32},
	"ecs.gn5-c8g1.2xlarge":   Spec{8, 60},
	"ecs.gn5-c4g1.2xlarge":   Spec{8, 60},
	"ecs.gn5-c8g1.4xlarge":   Spec{16, 120},
	"ecs.gn5-c28g1.7xlarge":  Spec{28, 112},
	"ecs.gn5-c8g1.8xlarge":   Spec{32, 240},
	"ecs.gn5-c28g1.14xlarge": Spec{56, 224},
	"ecs.gn5-c8g1.14xlarge":  Spec{54, 480},

	/* GPU 计算型实例规格族 gn5i
	   采用 NVIDIA P4 GPU 计算卡
	   处理器与内存配比为 1:4
	   处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell）
	   实例网络性能与计算规格对应（规格越高网络性能强）
	   适用场景：
	       深度学习推理
	       多媒体编解码等服务器端 GPU 计算工作负载

	*/
	"ecs.gn5i-c2g1.large":     Spec{2, 8},
	"ecs.gn5i-c4g1.xlarge":    Spec{4, 16},
	"ecs.gn5i-c8g1.2xlarge":   Spec{8, 32},
	"ecs.gn5i-c16g1.4xlarge":  Spec{16, 64},
	"ecs.gn5i-c28g1.14xlarge": Spec{56, 224},

	/* GPU 计算实例规格族 gn4
	   采用 NVIDIA M40 GPU 计算卡
	   多种 CPU 和 Memory 配比
	   处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell）
	   实例网络性能与计算规格对应（规格越高网络性能强）
	   适用场景
	       深度学习
	       科学计算，如计算流体动力学、计算金融学、基因组学研究、环境分析
	       高性能计算、渲染、多媒体编解码及其他服务器端 GPU 计算工作负载
	*/
	"ecs.gn4-c4g1.xlarge":  Spec{4, 30},
	"ecs.gn4-c8g1.2xlarge": Spec{8, 60},
	"ecs.gn4.8xlarge":      Spec{32, 48},
	"ecs.gn4-c4g1.2xlarge": Spec{8, 60},
	"ecs.gn4-c8g1.4xlarge": Spec{16, 60},
	"ecs.gn4.14xlarge":     Spec{56, 96},

	/* GPU 可视化计算型实例规格族 ga1
	   采用 AMD S7150 GPU 计算卡
	   CPU 和 Memory 配比为 1:2.5
	   处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell）
	   高性能 NVMe SSD 本地盘
	   实例网络性能与计算规格对应（规格越高网络性能强）
	   适用场景：
	       渲染、多媒体编解码
	       机器学习、高性能计算、高性能数据库
	       其他需要强大并行浮点计算能力的服务器端业务
	*/
	"ecs.ga1.2xlarge":  Spec{8, 20},
	"ecs.ga1.4xlarge":  Spec{16, 40},
	"ecs.ga1.8xlarge":  Spec{32, 80},
	"ecs.ga1.14xlarge": Spec{56, 160},

	/* FPGA 计算型实例规格族 f1
	   采用 Intel ARRIA 10 GX 1150 计算卡
	   CPU 和 Memory 配比为 1:7.5
	   处理器：2.5 GHz 主频的 Intel Xeon E5-2682 v4（Broadwell）
	   高性能 NVMe SSD 本地盘
	   实例网络性能与计算规格对应（规格越高网络性能强）
	   适用场景：
	       深度学习推理
	       基因组学研究、金融分析
	       实时视频处理及安全等计算工作负载
	*/
	"ecs.f1-c8f1.2xlarge": Spec{8, 60},
	"ecs.f1-c8f1.4xlarge": Spec{16, 120},

	/* 通用型神龙云服务器规格族 ebmg5
	   处理器与内存配比为 1:4
	   处理器：2.5 GHz 主频的 Intel Xeon Platinum 8163（Skylake），96 vCPU，最大睿频 3.1 GHz
	   高网络性能，450 万 PPS 网络收发包能力
	   支持部署 ZStack、OpenStack 等专有云服务
	   支持 SSD 云盘和高效云盘
	   适用场景：
	       高网络包收发场景，如视频弹幕、电信业务转发等
	       各种类型和规模的企业级应用
	       中大型数据库系统、缓存、搜索集群
	       数据分析和计算
	       计算集群、依赖内存的数据处理
	*/
	"ecs.ebmg5.24xlarge": Spec{96, 384},

	/* 超级计算集群计算型实例规格族 scc
	   同时支持 RoCE 网络和 VPC 网络：
	       RoCE 网络：2 * 25 Gbps 端口，专用于 RDMA 通信
	       VPC 网络：2 * 10 Gpbs 端口
	   具备弹性物理机的所有特性
	   由 Intel Xeon Platinum 8163（Skylake）CPU 和 NVIDIA P100 GPU 提供极致计算动力
	   处理器与内存配比有 1:3、1:4 和 1:8
	   适用场景：
	       大规模机器学习应用
	       高性能科学和工程应用
	       数据分析、批量计算、视频编码
	       高网络包收发场景，如视频弹幕、电信业务转发等

	*/
	"ecs.scch5.16xlarge": Spec{64, 192},
	"ecs.sccg5.24xlarge": Spec{96, 384},

	// 入门级 X86 计算规格族
	/* 突发性能实例 t5
	   处理器：2.5 GHz 主频的 Intel Xeon Platinum 8163（Skylake）
	   搭配 DDR4 内存
	   多种处理器和内存配比
	   可突然提速的 vCPU，持续基本性能，受到 vCPU 积分的限制
	   计算、内存和网络资源的平衡
	   适用场景：
	       Web应用服务器
	       轻负载应用、微服务
	       开发测试压测服务应用
	*/
	//"t5-lc2m1.nano":   Spec{1, 0.5},
	"t5-lc1m1.small":  Spec{1, 1},
	"t5-lc1m2.small":  Spec{1, 2},
	"t5-lc1m2.large":  Spec{2, 4},
	"t5-lc1m4.large":  Spec{2, 8},
	"t5-c1m1.large":   Spec{2, 2},
	"t5-c1m2.large":   Spec{2, 4},
	"t5-c1m4.largee":  Spec{2, 8},
	"t5-c1m1.xlarge":  Spec{4, 4},
	"t5-c1m2.xlarge":  Spec{4, 8},
	"t5-c1m4.xlarge":  Spec{4, 16},
	"t5-c1m1.2xlarge": Spec{8, 8},
	"t5-c1m2.2xlarge": Spec{8, 16},
	"t5-c1m4.2xlarge": Spec{8, 32},

	"ecs.xn4.small": Spec{1, 1},

	"ecs.n4.small":   Spec{1, 2},
	"ecs.n4.large":   Spec{2, 4},
	"ecs.n4.xlarge":  Spec{4, 8},
	"ecs.n4.2xlarge": Spec{8, 16},
	"ecs.n4.4xlarge": Spec{16, 32},
	"ecs.n4.8xlarge": Spec{32, 64},

	"ecs.mn4.small":   Spec{1, 4},
	"ecs.mn4.large":   Spec{2, 8},
	"ecs.mn4.xlarge":  Spec{4, 16},
	"ecs.mn4.2xlarge": Spec{8, 32},
	"ecs.mn4.4xlarge": Spec{16, 64},

	"ecs.e4.small": Spec{1, 8},

	"ecs.n1.tiny":    Spec{1, 1},
	"ecs.n1.small":   Spec{1, 2},
	"ecs.n1.medium":  Spec{2, 4},
	"ecs.n1.large":   Spec{4, 8},
	"ecs.n1.xlarge":  Spec{8, 16},
	"ecs.n1.3xlarge": Spec{16, 32},
	"ecs.n1.7xlarge": Spec{32, 64},

	"ecs.n2.small":   Spec{1, 4},
	"ecs.n2.medium":  Spec{2, 8},
	"ecs.n2.large":   Spec{4, 16},
	"ecs.n2.xlarge":  Spec{8, 32},
	"ecs.n2.3xlarge": Spec{16, 64},
	"ecs.n2.7xlarge": Spec{32, 128},

	"ecs.e3.small":   Spec{1, 8},
	"ecs.e3.medium":  Spec{2, 16},
	"ecs.e3.large":   Spec{4, 32},
	"ecs.e3.xlarge":  Spec{8, 64},
	"ecs.e3.3xlarge": Spec{16, 128},
}

func IsMachineTypeExist(t string) bool {
	if _, exist := MachineSpec[t]; exist {
		return true
	}
	return false
}

func ListMachineSpecStr() []string {
	ret := make([]string, 0, len(MachineSpec))
	for name, spec := range MachineSpec {
		ret = append(ret, fmt.Sprintf("%s:(%d vCPU %d GB)", name, spec.NumCpu, spec.MemGB))
	}
	return ret
}

type ProviderInfo struct {
	CreateMachinePassword string `json:"createMachinePassword"`
	CreateMachineType     string `json:"createMachineType"`
}
