# Getting Started
## Prerequisites
- Aliyun account accesskeyid and accesskeysecret.
- Create a private operation system image which can automatically join your k8s cluster.

## Build
```
go build -o publiccloudprovider cmd/cloudprovider/publiccloudprovider.go
go build -o clustermanager cmd/clustermanager/clustermanager.go
```
## Run publiccloudprovider
```
./publiccloudprovider --ali-accesskeyid=$youraccesskeyid --ali-accesskeysecret=$youraccesskeysecret --ali-securitygroupid=$yoursecuritygroupid  --ali-imageid=$yourimageid --ali-instancetype=ecs.xn4.small --httpaddr=0.0.0.0:8087
```
## Run clustermanager
```
./clustermanager --kubeconfig=/config --createrequestcpuabove=92 --deleterequestcpuunder=72 --deployUrl=http://0.0.0.0:8086 --maxcreatenode=3 --clustercheckinterval=10s  --providerurl=http://127.0.0.1:8087 
```

