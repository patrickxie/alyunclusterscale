package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	// DeletedPodsCount tracks the number of deletion of pods in order to schedule a critical one.
	DeletedMachineCount = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "yunprovider",
			Name:      "deleted_machine_count",
			Help:      "Number of machines deleted by yunprovider.",
		})

	StopedMachineCount = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "yunprovider",
			Name:      "stoped_machine_count",
			Help:      "Number of machines stoped by yunprovider.",
		})

	RestartMachineCount = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "yunprovider",
			Name:      "restarted_machine_count",
			Help:      "Number of machines restarted by yunprovider.",
		})

	StartMachineCount = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "yunprovider",
			Name:      "started_machine_count",
			Help:      "Number of machines started by yunprovider.",
		})

	CreatedMachineCount = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "yunprovider",
			Name:      "created_machine_count",
			Help:      "Number of machines created by yunprovider.",
		})
)

func init() {
	prometheus.MustRegister(DeletedMachineCount)
	prometheus.MustRegister(StopedMachineCount)
	prometheus.MustRegister(RestartMachineCount)
	prometheus.MustRegister(StartMachineCount)
	prometheus.MustRegister(CreatedMachineCount)
}
