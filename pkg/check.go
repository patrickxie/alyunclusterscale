package pkg

import (
	"fmt"
	"yunprovider/cmd/cloudprovider/app/options"
)

func CheckConfig(config *options.PublicClouldProviderConfig) error {
	switch config.Type {
	case options.UnKnowType:
		return fmt.Errorf("UnKnow Clound Provide")
	case options.AliYun:
		return checkAliyun(&config.AliYunConfig)
	default:
		return fmt.Errorf("UnKnow Clound Provide")
	}
}

func checkAliyun(config *options.AliYunConfig) error {
	if config.AliYun_AccessKeyID == "" {
		return fmt.Errorf("ali-accesskeyid should not be empty")
	}
	if config.AliYun_AccessKeySecret == "" {
		return fmt.Errorf("ali-accesskeysecret should not be empty")
	}
	if config.AliYun_SecurityGroupId == "" {
		return fmt.Errorf("ali-securitygroupid should not be empty")
	}
	return nil
}
