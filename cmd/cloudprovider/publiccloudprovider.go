package main

import (
	"fmt"
	"os"
	"yunprovider/cmd/cloudprovider/app"
	"yunprovider/cmd/cloudprovider/app/options"
	"yunprovider/version"

	"github.com/golang/glog"
	"github.com/spf13/pflag"
)

/*
 ./publiccloudprovider \
    --ali-accesskeyid=LTAIyQdy7mQJGaZj \
    --ali-accesskeysecret=TopuzeX8kAM3cYB8f1nFy3mh1LpaXS \
    --ali-securitygroupid=sg-uf6h3xsclturcombjxfx \
    --allowhttpmanagemachine=true \
    --ali-imageid=m-uf6c75kyx5u783p1c8nb \
    --ali-instancetype=ecs.xn4.small \
    --httpaddr=127.0.0.1:8087
*/

func main() {
	provider := options.NewPublicClouldProvider()
	provider.AddFlags(pflag.CommandLine)

	options.InitFlags()
	defer glog.Flush()

	version.PrintAndExitIfRequested()
	if err := app.Run(provider); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}
