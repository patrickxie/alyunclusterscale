package options

import (
	"fmt"
	yunprovidercommon "yunprovider/common"

	"github.com/denverdino/aliyungo/common"
	"github.com/denverdino/aliyungo/ecs"
	"github.com/spf13/pflag"
)

type ProviderType int

const (
	UnKnowType = iota
	AliYun
)

type AliYunConfig struct {
	AliYun_AccessKeyID                    string
	AliYun_AccessKeySecret                string
	AliYun_Regions                        []string
	AliYun_CreateImageId                  string
	AliYun_InstanceType                   string
	AliYun_SecurityGroupId                string
	AliYun_InternetMaxBandwidthIn         int
	AliYun_InternetMaxBandwidthOut        int
	AliYun_InstanceDefaultPassword        string
	AliYun_InstanceSystemDiskSize         int
	AliYun_InstanceSystemDiskCategory     string
	AliYun_InstanceDataDiskNum            int
	AliYun_InstanceDataDiskSize           int
	AliYun_InstanceDataDiskCategory       string
	AliYun_InstanceDataRemoveWhenVMDelete bool
}

func (ali *AliYunConfig) AddFlags(fs *pflag.FlagSet) {
	fs.StringVar(&ali.AliYun_AccessKeyID, "ali-accesskeyid", ali.AliYun_AccessKeyID,
		"Should not empty, it is used to connect aliyun api.")
	fs.StringVar(&ali.AliYun_AccessKeySecret, "ali-accesskeysecret", ali.AliYun_AccessKeySecret,
		"Should not empty, it is used to connect aliyun api.")
	fs.StringSliceVar(&ali.AliYun_Regions, "ali-regions", ali.AliYun_Regions, ""+
		"The region which virtual machine will be create at.")
	fs.StringVar(&ali.AliYun_CreateImageId, "ali-imageid", ali.AliYun_CreateImageId, ""+
		"Image to create virtual machine.")
	fs.StringVar(&ali.AliYun_InstanceType, "ali-instancetype", ali.AliYun_InstanceType, ""+
		fmt.Sprintf("The create virtual machine type %v.", yunprovidercommon.ListMachineSpecStr()))
	fs.StringVar(&ali.AliYun_SecurityGroupId, "ali-securitygroupid", ali.AliYun_SecurityGroupId, ""+
		"The create virtual machine security group id.")
	fs.IntVar(&ali.AliYun_InternetMaxBandwidthIn, "ali-internetmaxbandwidthin", ali.AliYun_InternetMaxBandwidthIn, ""+
		"Max bandwidth to virtual machine [1-200]Mbps.")
	fs.IntVar(&ali.AliYun_InternetMaxBandwidthOut, "ali-internetmaxbandwidthout", ali.AliYun_InternetMaxBandwidthOut, ""+
		"Max bandwidth from virtual machine to public internet[0-100]Mbps.")
	fs.StringVar(&ali.AliYun_InstanceDefaultPassword, "ali-vmdefaultpassword", ali.AliYun_InstanceDefaultPassword, ""+
		"The create virtual machine defautl root password.")
	fs.IntVar(&ali.AliYun_InstanceSystemDiskSize, "ali-systemdisksize", ali.AliYun_InstanceSystemDiskSize, ""+
		"The create virtual machine system disk size[cloud − 40~500GB, cloud_efficiency − 40~500, cloud_ssd − 40~500, ephemeral_ssd − 40~500].")
	fs.StringVar(&ali.AliYun_InstanceSystemDiskCategory, "ali-systemdiskcategory", ali.AliYun_InstanceSystemDiskCategory, ""+
		"The create virtual machine system disk category[cloud, cloud_efficiency, cloud_ssd, ephemeral_ssd].")
	fs.IntVar(&ali.AliYun_InstanceDataDiskNum, "ali-datadisknum", ali.AliYun_InstanceDataDiskNum, ""+
		"The create virtual machine data disk number.")
	fs.IntVar(&ali.AliYun_InstanceDataDiskSize, "ali-datadisksize", ali.AliYun_InstanceDataDiskSize, ""+
		"The create virtual machine data disk size [cloud：5 ~ 2000, cloud_efficiency：20 ~ 32768, cloud_ssd：20 ~ 32768, ephemeral_ssd：5 ~ 800].")
	fs.StringVar(&ali.AliYun_InstanceDataDiskCategory, "ali-datadiskcategory", ali.AliYun_InstanceDataDiskCategory, ""+
		"The create virtual machine data disk category [cloud, cloud_efficiency, cloud_ssd, ephemeral_ssd].")
	fs.BoolVar(&ali.AliYun_InstanceDataRemoveWhenVMDelete, "ali-datadiskremove", ali.AliYun_InstanceDataRemoveWhenVMDelete, ""+
		"If true the data disk will be deleted when the mount vm is deleted")
}

type PublicClouldProviderConfig struct {
	AliYunConfig
	Type ProviderType
}

type PublicClouldProvider struct {
	PublicClouldProviderConfig
	HttpServerAddr         string
	AllowHttpManageMachine bool
	//KubeConfig flag.StringFlag
}

func NewPublicClouldProvider() *PublicClouldProvider {
	return &PublicClouldProvider{
		HttpServerAddr:         ":8087",
		AllowHttpManageMachine: false,
		PublicClouldProviderConfig: PublicClouldProviderConfig{
			AliYunConfig: AliYunConfig{
				AliYun_AccessKeyID:                    "",
				AliYun_AccessKeySecret:                "",
				AliYun_Regions:                        []string{string(common.Shanghai)},
				AliYun_CreateImageId:                  "centos_7_03_64_40G_alibase_20170710.vhd",
				AliYun_InstanceType:                   "ecs.xn4.small",
				AliYun_SecurityGroupId:                "",
				AliYun_InternetMaxBandwidthIn:         20,
				AliYun_InternetMaxBandwidthOut:        20,
				AliYun_InstanceDefaultPassword:        "Enndata123.com",
				AliYun_InstanceSystemDiskSize:         50,
				AliYun_InstanceSystemDiskCategory:     string(ecs.DiskCategoryCloudEfficiency),
				AliYun_InstanceDataDiskNum:            0,
				AliYun_InstanceDataDiskSize:           50,
				AliYun_InstanceDataDiskCategory:       string(ecs.DiskCategoryCloudEfficiency),
				AliYun_InstanceDataRemoveWhenVMDelete: true,
			},
		},
		//KubeConfig: flag.NewStringFlag("/var/lib/kubelet/kubeconfig"),
	}
}

func (p *PublicClouldProvider) AddFlags(fs *pflag.FlagSet) {
	fs.StringVar(&p.HttpServerAddr, "httpaddr", p.HttpServerAddr, ""+
		"Http server addr")
	fs.BoolVar(&p.AllowHttpManageMachine, "allowhttpmanagemachine", p.AllowHttpManageMachine, ""+
		"If true can use httpserver:/create to create a new machine")

	p.PublicClouldProviderConfig.AliYunConfig.AddFlags(fs)
	p.Type = AliYun
	if p.PublicClouldProviderConfig.AliYunConfig.AliYun_AccessKeyID != "" &&
		p.PublicClouldProviderConfig.AliYunConfig.AliYun_AccessKeySecret != "" {
		p.Type = AliYun
	}
}

func InitFlags() {
	pflag.Parse()

}
