package app

import (
	"fmt"
	"yunprovider/cmd/cloudprovider/app/options"
	"yunprovider/pkg"
)

func Run(s *options.PublicClouldProvider) error {
	if err := run(s); err != nil {
		return fmt.Errorf("failed to run PublicClouldProvider: %v", err)

	}
	return nil
}

func run(s *options.PublicClouldProvider) (err error) {
	errCheck := pkg.CheckConfig(&s.PublicClouldProviderConfig)
	if errCheck != nil {
		return fmt.Errorf("Check config err:%v", errCheck)
	}

	provider := pkg.NewProvider(s)
	if provider == nil {
		return fmt.Errorf("NewProvider fail")
	}

	runErr := pkg.RunProvider(provider, s)

	return runErr
}
