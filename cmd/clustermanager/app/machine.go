package app

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
	"yunprovider/common"
)

const (
	ProviderCreateNodePrifx string = "k8sprovider"
)

func UrlJoin(url string, args ...string) string {
	if strings.HasSuffix(url, "/") == true {
		url = url[:len(url)-1]
	}
	for _, a := range args {
		url += "/" + a
	}
	return url
}

func getMachineNameAndHostnameByIndex(index int) (string, string) {
	name := fmt.Sprintf("%s-%d", ProviderCreateNodePrifx, index)
	return name, name
}

func GetCreatedMachine(url string) ([]common.YunMachine, error) {
	ret := []common.YunMachine{}
	resq, err := http.Get(UrlJoin(url, "machines"))
	if err != nil {
		return ret, err
	}
	defer resq.Body.Close()
	if resq.StatusCode != http.StatusOK {
		return ret, fmt.Errorf("GetCreatedMachine %s, code:%d", url, resq.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resq.Body)
	if errRead != nil {
		return ret, fmt.Errorf("GetCreatedMachine %s, readErr:%v", url, errRead)
	}
	bufStr := strings.Replace(string(buf), "\n", "", -1)
	resItem := GetRetItem{}
	errJson := json.Unmarshal([]byte(bufStr), &resItem)
	if errJson != nil {
		return ret, fmt.Errorf("GetCreatedMachine buf:%s Unmarshal error:%v", url, bufStr, errJson)
	}

	machines := []common.YunMachine{}
	errJson = json.Unmarshal([]byte(resItem.Data), &machines)
	if errJson != nil {
		return ret, fmt.Errorf("GetCreatedMachine buf:%s Unmarshal error:%v", url, string(buf), errJson)
	}

	for _, m := range machines {
		if strings.HasPrefix(m.Name, ProviderCreateNodePrifx) != true {
			continue
		}
		ret = append(ret, m)
	}
	return ret, nil
}

func GetMachine(url, machinename string) (common.YunMachine, error) {
	resq, err := http.Get(UrlJoin(url, "machines", machinename))
	if err != nil {
		return common.YunMachine{}, err
	}
	defer resq.Body.Close()
	if resq.StatusCode != http.StatusOK {
		return common.YunMachine{}, fmt.Errorf("GetMachine %s, code:%d", url, resq.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resq.Body)
	if errRead != nil {
		return common.YunMachine{}, fmt.Errorf("GetMachine %s, readErr:%v", url, errRead)
	}

	bufStr := strings.Replace(string(buf), "\n", "", -1)
	retItem := GetRetItem{}
	errJson := json.Unmarshal([]byte(bufStr), &retItem)
	if errJson != nil {
		return common.YunMachine{}, fmt.Errorf("GetMachine buf:%s Unmarshal error:%v", url, bufStr, errJson)
	}

	if retItem.Code != 200 {
		return common.YunMachine{}, fmt.Errorf("get error:%s", retItem.Err)
	}

	machine := common.YunMachine{}
	errJson = json.Unmarshal([]byte(retItem.Data), &machine)
	if errJson != nil {
		return machine, errJson
	}
	return machine, nil
}

func CreateMachine(url string, machinename, hostname string) (common.YunMachine, error) {
	tmpHostname := machinename
	if hostname != "" {
		tmpHostname = hostname
	}
	resq, err := http.Get(UrlJoin(url, "create", machinename, tmpHostname))
	if err != nil {
		return common.YunMachine{}, err
	}
	defer resq.Body.Close()
	if resq.StatusCode != http.StatusOK {
		return common.YunMachine{}, fmt.Errorf("CreateMachine %s, code:%d", url, resq.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resq.Body)
	if errRead != nil {
		return common.YunMachine{}, fmt.Errorf("GetCreatedNodeInfo %s, readErr:%v", url, errRead)
	}

	bufStr := strings.Replace(string(buf), "\n", "", -1)
	retItem := GetRetItem{}
	errJson := json.Unmarshal([]byte(bufStr), &retItem)
	if errJson != nil {
		return common.YunMachine{}, fmt.Errorf("GetCreatedNodeInfo buf:%s Unmarshal error:%v", url, bufStr, errJson)
	}

	if retItem.Code != 200 {
		return common.YunMachine{}, fmt.Errorf("create error:%s", retItem.Err)
	}

	machine := common.YunMachine{}
	errJson = json.Unmarshal([]byte(retItem.Data), &machine)
	if errJson != nil {
		return machine, errJson
	}

	if hostname == "" {
		machine := common.YunMachine{}
		errJson = json.Unmarshal([]byte(retItem.Data), &machine)
		if errJson != nil {
			return machine, fmt.Errorf("CreateMachine jsonerr:%v", errJson)
		}
		return machine, ChangeMachinesHostName(url, machinename, machine.InnerIP)
	}
	return machine, nil
}

func StartMachine(url string, machinename string) (common.YunMachine, error) {
	resq, err := http.Get(UrlJoin(url, "start", machinename))
	if err != nil {
		return common.YunMachine{}, err
	}
	defer resq.Body.Close()
	if resq.StatusCode != http.StatusOK {
		return common.YunMachine{}, fmt.Errorf("StartMachine %s, code:%d", url, resq.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resq.Body)
	if errRead != nil {
		return common.YunMachine{}, fmt.Errorf("StartMachine %s, readErr:%v", url, errRead)
	}

	bufStr := strings.Replace(string(buf), "\n", "", -1)
	retItem := GetRetItem{}
	errJson := json.Unmarshal([]byte(bufStr), &retItem)
	if errJson != nil {
		return common.YunMachine{}, fmt.Errorf("StartMachine buf:%s Unmarshal error:%v", url, bufStr, errJson)
	}

	if retItem.Code != 200 {
		return common.YunMachine{}, fmt.Errorf("start error:%s", retItem.Err)
	}

	machine := common.YunMachine{}
	errJson = json.Unmarshal([]byte(retItem.Data), &machine)
	if errJson != nil {
		return machine, errJson
	}
	return machine, nil
}

func RestartMachine(url string, machinename string) error {
	resq, err := http.Get(UrlJoin(url, "restart", machinename))
	if err != nil {
		return err
	}
	defer resq.Body.Close()
	if resq.StatusCode != http.StatusOK {
		return fmt.Errorf("RestartMachine %s, code:%d", url, resq.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resq.Body)
	if errRead != nil {
		return fmt.Errorf("RestartMachine %s, readErr:%v", url, errRead)
	}

	bufStr := strings.Replace(string(buf), "\n", "", -1)
	retItem := GetRetItem{}
	errJson := json.Unmarshal([]byte(bufStr), &retItem)
	if errJson != nil {
		return fmt.Errorf("RestartMachine buf:%s Unmarshal error:%v", url, bufStr, errJson)
	}

	if retItem.Code != 200 {
		return fmt.Errorf("restart error:%s", retItem.Err)
	}
	return nil
}

func DeployMachine(url string, ip, password string) error {
	resq, err := http.Get(UrlJoin(url, "deploy", ip, password))
	if err != nil {
		return err
	}
	defer resq.Body.Close()
	if resq.StatusCode != http.StatusOK {
		return fmt.Errorf("DeployMachine %s, code:%d", url, resq.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resq.Body)
	if errRead != nil {
		return fmt.Errorf("DeployMachine %s, readErr:%v", url, errRead)
	}

	bufStr := strings.Replace(string(buf), "\n", "", -1)
	retItem := GetRetItem{}
	errJson := json.Unmarshal([]byte(bufStr), &retItem)
	if errJson != nil {
		return fmt.Errorf("RestartMachine buf:%s Unmarshal error:%v", url, bufStr, errJson)
	}

	if retItem.Code != 200 {
		return fmt.Errorf("restart error:%s", retItem.Err)
	}
	return nil
}

func ChangeMachinesHostName(url, machinename, newhostname string) error {
	resq, err := http.Get(UrlJoin(url, "machines", "hostname", machinename, newhostname))
	if err != nil {
		return err
	}
	defer resq.Body.Close()
	if resq.StatusCode != http.StatusOK {
		return fmt.Errorf("ChangeMachinesHostName %s, code:%d", url, resq.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resq.Body)
	if errRead != nil {
		return fmt.Errorf("ChangeMachinesHostName %s, readErr:%v", url, errRead)
	}

	bufStr := strings.Replace(string(buf), "\n", "", -1)
	retItem := GetRetItem{}
	errJson := json.Unmarshal([]byte(bufStr), &retItem)
	if errJson != nil {
		return fmt.Errorf("ChangeMachinesHostName buf:%s Unmarshal error:%v", url, bufStr, errJson)
	}

	if retItem.Code != 200 {
		return fmt.Errorf("ChangeMachinesHostName error:%s", retItem.Err)
	}
	return nil
}

func StopMachine(url string, machinename string) error {
	resq, err := http.Get(UrlJoin(url, "stop", machinename))
	if err != nil {
		return err
	}
	defer resq.Body.Close()
	if resq.StatusCode != http.StatusOK {
		return fmt.Errorf("StopMachine %s, code:%d", url, resq.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resq.Body)
	if errRead != nil {
		return fmt.Errorf("StopMachine %s, readErr:%v", url, errRead)
	}

	bufStr := strings.Replace(string(buf), "\n", "", -1)
	retItem := GetRetItem{}
	errJson := json.Unmarshal([]byte(bufStr), &retItem)
	if errJson != nil {
		return fmt.Errorf("StopMachine buf:%s Unmarshal error:%v", url, bufStr, errJson)
	}

	if retItem.Code != 200 {
		return fmt.Errorf("stop error:%s", retItem.Err)
	}
	return nil
}

func DeleteMachine(url string, machinename string) error {
	resq, err := http.Get(UrlJoin(url, "delete", machinename))
	if err != nil {
		return err
	}
	defer resq.Body.Close()
	if resq.StatusCode != http.StatusOK {
		return fmt.Errorf("DeleteMachine %s, code:%d", url, resq.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resq.Body)
	if errRead != nil {
		return fmt.Errorf("DeleteMachine %s, readErr:%v", url, errRead)
	}

	bufStr := strings.Replace(string(buf), "\n", "", -1)
	retItem := GetRetItem{}
	errJson := json.Unmarshal([]byte(bufStr), &retItem)
	if errJson != nil {
		return fmt.Errorf("DeleteMachine buf:%s Unmarshal error:%v", url, bufStr, errJson)
	}

	if retItem.Code != 200 {
		return fmt.Errorf("delete error:%s", retItem.Err)
	}
	return nil
}

func UpdateCreateMachineType(url string, providername, machinename string) error {
	resq, err := http.Get(UrlJoin(url, "setcreatemachinetype", providername, machinename))
	if err != nil {
		return err
	}
	defer resq.Body.Close()
	if resq.StatusCode != http.StatusOK {
		return fmt.Errorf("UpdateCreateMachineType %s, code:%d", url, resq.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resq.Body)
	if errRead != nil {
		return fmt.Errorf("UpdateCreateMachineType %s, readErr:%v", url, errRead)
	}

	bufStr := strings.Replace(string(buf), "\n", "", -1)
	retItem := GetRetItem{}
	errJson := json.Unmarshal([]byte(bufStr), &retItem)
	if errJson != nil {
		return fmt.Errorf("UpdateCreateMachineType buf:%s Unmarshal error:%v", url, bufStr, errJson)
	}

	if retItem.Code != 200 {
		return fmt.Errorf("update error:%s", retItem.Err)
	}
	return nil
}

func IsProviderHealth(url string) error {
	resq, err := http.Get(UrlJoin(url, "health"))
	if err != nil {
		return err
	}
	defer resq.Body.Close()
	if resq.StatusCode != http.StatusOK {
		return fmt.Errorf("IsProviderHealth %s, code:%d", url, resq.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resq.Body)
	if errRead != nil {
		return fmt.Errorf("IsProviderHealth %s, readErr:%v", url, errRead)
	}
	if strings.ToLower(string(buf)) != "ok" {
		return fmt.Errorf("IsProviderHealth %s, read %s is not ok", url, string(buf))
	}
	return nil
}

func GetProviderInfo(url string) (common.ProviderInfo, error) {
	resq, err := http.Get(UrlJoin(url, "machines", "info"))
	if err != nil {
		return common.ProviderInfo{}, err
	}
	defer resq.Body.Close()
	if resq.StatusCode != http.StatusOK {
		return common.ProviderInfo{}, fmt.Errorf("GetProviderInfo %s, code:%d", url, resq.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resq.Body)
	if errRead != nil {
		return common.ProviderInfo{}, fmt.Errorf("GetProviderInfo %s, readErr:%v", url, errRead)
	}

	ret := common.ProviderInfo{}
	errJson := json.Unmarshal(buf, &ret)
	if errJson != nil {
		return ret, fmt.Errorf("GetProviderInfo buf:%s Unmarshal error:%v", string(buf), errJson)
	}

	return ret, nil
}

func RecycleMachine(url string, machine common.YunMachine, cur, tryTime int, duration time.Duration) error {
	newMachine, err := GetMachine(url, machine.Name)
	if err != nil {
		return err
	}
	switch newMachine.Status {
	case common.MachineRunning:
		errStop := StopMachine(url, newMachine.Name)
		if errStop != nil {
			if cur <= tryTime {
				time.Sleep(duration)
				return RecycleMachine(url, newMachine, cur+1, tryTime, duration)
			}
			return errStop
		}
		cur = 0
		fallthrough
	case common.MachineStopped:
		errDelete := DeleteMachine(url, newMachine.Name)
		if errDelete != nil {
			if cur <= tryTime {
				time.Sleep(duration)
				return RecycleMachine(url, newMachine, cur+1, tryTime, duration)
			}
			return errDelete
		}
	default:
		if cur <= tryTime {
			time.Sleep(duration)
			return RecycleMachine(url, newMachine, cur+1, tryTime, duration)
		}
		return fmt.Errorf("machine status is %s cannot be recycled", newMachine.Status)
	}
	return nil
}

func RebootMachine(url string, machine common.YunMachine) error {
	switch machine.Status {
	case common.MachineStarting:
		return nil
	case common.MachineRunning:
		errRestart := RestartMachine(url, machine.Name)
		if errRestart != nil {
			return errRestart
		}
	case common.MachineStopped:
		_, errStart := StartMachine(url, machine.Name)
		if errStart != nil {
			return errStart
		}
	default:
		return fmt.Errorf("machine status is %s cannot be reboot", machine.Status)
	}
	return nil
}
