package options

import (
	"time"

	"github.com/spf13/pflag"
)

type ClusterManagerConfig struct {
	ClusterCheckInterval   time.Duration
	MaxCreateNode          int
	MachineDeployTimeout   time.Duration
	MachineRestartInterval time.Duration
	ProviderUrl            string
	DeployUrl              string
	NeedDeploy             bool
	MachinePassword        string
	APIMaster              string
	Kubeconfig             string
	ContentType            string
	CreateRequestCpuAbove  float64
	CreateLimitCpuAbove    float64
	CreateRequestMemAbove  float64
	CreateLimitMemAbove    float64
	DeleteRequestCpuUnder  float64
	DeleteLimitCpuUnder    float64
	DeleteRequestMemUnder  float64
	DeleteLimitMemUnder    float64
	DeletePaddingDuration  time.Duration

	// ServerConfig enables above parameters by web interface
	ServerConfig
}

type ServerConfig struct {
	ConfigUrl      string
	ServerCertPath string
	ServerKeyPath  string
	BaseAuthFile   string
}

func (cmc *ClusterManagerConfig) AddFlags(fs *pflag.FlagSet) {
	fs.StringVar(&cmc.APIMaster, "apimaster", cmc.APIMaster,
		"The address of the Kubernetes API server (overrides any value in kubeconfig)")
	fs.StringVar(&cmc.Kubeconfig, "kubeconfig", cmc.Kubeconfig,
		"Path to kubeconfig file with authorization and master location information.")
	fs.StringVar(&cmc.ProviderUrl, "providerurl", cmc.ProviderUrl,
		"PublicCloudProvider url.")
	fs.StringVar(&cmc.DeployUrl, "deployUrl", cmc.DeployUrl,
		"Deploy machine url.")

	fs.BoolVar(&cmc.NeedDeploy, "needdeploy", cmc.NeedDeploy,
		"When create a machine if need deploy use ansible.")
	fs.StringVar(&cmc.MachinePassword, "machinepassword", cmc.MachinePassword,
		"The password to login machine.")
	fs.StringVar(&cmc.ContentType, "kube-api-content-type", cmc.ContentType, "Content type of requests sent to apiserver.")
	fs.Float64Var(&cmc.CreateRequestCpuAbove, "createrequestcpuabove", cmc.CreateRequestCpuAbove, "Create new node when cluster request cpu above createrequestcpuabove")
	fs.Float64Var(&cmc.CreateLimitCpuAbove, "createlimitcpuabove", cmc.CreateLimitCpuAbove, "Create new node when cluster limit cpu above createlimitcpuabove")
	fs.Float64Var(&cmc.CreateRequestMemAbove, "createrequestmemabove", cmc.CreateRequestMemAbove, "Create new node when cluster request memory above createrequestmemabove")
	fs.Float64Var(&cmc.CreateLimitMemAbove, "createlimitmemabove", cmc.CreateLimitMemAbove, "Create new node when cluster limit memory above createlimitmemabove")

	fs.Float64Var(&cmc.DeleteRequestCpuUnder, "deleterequestcpuunder", cmc.DeleteRequestCpuUnder, "Delete one node when cluster request cpu under deleterequestcpuunder")
	fs.Float64Var(&cmc.DeleteLimitCpuUnder, "deletelimitcpuunder", cmc.DeleteLimitCpuUnder, "Delete one node when cluster limit cpu under deletelimitcpuunder")
	fs.Float64Var(&cmc.DeleteRequestMemUnder, "deleterequestmemunder", cmc.DeleteRequestMemUnder, "Delete one node when cluster request memory under deleterequestmemunder")
	fs.Float64Var(&cmc.DeleteLimitMemUnder, "deletelimitmemunder", cmc.DeleteLimitMemUnder, "Delete one node when cluster limit memory under deletelimitmemunder")
	fs.IntVar(&cmc.MaxCreateNode, "maxcreatenode", cmc.MaxCreateNode, "The max node number can be created")
	fs.DurationVar(&cmc.MachineDeployTimeout, "machinedeploytimeout", cmc.MachineDeployTimeout, "Machine will by deleted after timeout not join cluster")
	fs.DurationVar(&cmc.MachineRestartInterval, "machinerestartinterval", cmc.MachineRestartInterval, "Machine restart interval for not ready or not join cluster")
	fs.DurationVar(&cmc.DeletePaddingDuration, "deletepaddingduration", cmc.DeletePaddingDuration, "Start recycle machine padding time")
	fs.DurationVar(&cmc.ClusterCheckInterval, "clustercheckinterval", cmc.ClusterCheckInterval, "Cluster state check interval")
	fs.StringVar(&cmc.ConfigUrl, "config-Url", cmc.ConfigUrl, "Dynamic config url.")
	fs.StringVar(&cmc.ServerCertPath, "config-servercertpath", "", "config web interface https server cert. host:port/config/get or host:port/config/set")
	fs.StringVar(&cmc.ServerKeyPath, "config-serverkeypath", "", "config web interface https server private key. host:port/config/get or host:port/config/set")
	fs.StringVar(&cmc.BaseAuthFile, "config-baseauthfile", "", "config web interface https server base auth. host:port/config/get or host:port/config/set")

}

func NewClusterManagerConfig() *ClusterManagerConfig {
	return &ClusterManagerConfig{
		ContentType: "application/vnd.kubernetes.protobuf",
		ProviderUrl: "http://127.0.0.1:8087",
		DeployUrl:   "http://127.0.0.1:8086",
		//ConfigUrl:              ":8085",
		NeedDeploy:             true,
		MachinePassword:        "Enndata123.com",
		CreateRequestCpuAbove:  -1.0,
		CreateLimitCpuAbove:    -1.0,
		CreateRequestMemAbove:  -1.0,
		CreateLimitMemAbove:    -1.0,
		DeleteRequestCpuUnder:  -1.0,
		DeleteLimitCpuUnder:    -1.0,
		DeleteRequestMemUnder:  -1.0,
		DeleteLimitMemUnder:    -1.0,
		MaxCreateNode:          1,
		MachineDeployTimeout:   10 * time.Minute,
		MachineRestartInterval: 100 * time.Second,
		DeletePaddingDuration:  10 * time.Minute,
		ClusterCheckInterval:   10 * time.Second,
	}
}

func InitFlags() {
	pflag.Parse()

}
