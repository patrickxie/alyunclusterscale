package app

import (
	"fmt"
	"strconv"
	"strings"
	"time"
	"yunprovider/common"

	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	apiv1 "k8s.io/kubernetes/pkg/api/v1"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

type ClusterInfo struct {
	TotalNode       int64
	TotalPod        int64
	TotalCpu        int64
	TotalMem        int64
	ProviderNodeNum int64
	ReadyNode       int64
	ReadyPod        int64
	RequestedCpu    int64
	RequestedMem    int64
	LimitedCpu      int64
	LimitedMem      int64
	NotReadyNode    int64
	NotReadyPod     int64
	RequestCpuRatio float64
	RequestMemRatio float64
	LimitCpuRatio   float64
	LimitMemRatio   float64
}

func isNodeReady(node *apiv1.Node) bool {
	if node == nil {
		return false
	}
	for _, c := range node.Status.Conditions {
		if c.Type == apiv1.NodeReady {
			if c.Status == apiv1.ConditionTrue {
				return true
			} else {
				return false
			}
		}
	}
	return false
}

func isPodReady(pod *apiv1.Pod) bool {
	if pod == nil {
		return false
	}
	if pod.Status.Phase != apiv1.PodRunning {
		return false
	}
	for _, c := range pod.Status.Conditions {
		if c.Type == apiv1.PodReady {
			if c.Status == apiv1.ConditionTrue {
				return true
			}
		}
	}
	return false
}

func getPodReqAndLimitRes(pod *apiv1.Pod) (reqCpu, reqMem, limitCpu, limitMem int64) {
	if pod == nil {
		return
	}
	for _, c := range pod.Spec.Containers {
		for rName, rQuantity := range c.Resources.Requests {
			switch rName {
			case apiv1.ResourceMemory:
				reqMem += rQuantity.Value()
			case apiv1.ResourceCPU:
				reqCpu += rQuantity.MilliValue()
			}
		}
		for rName, rQuantity := range c.Resources.Limits {
			switch rName {
			case apiv1.ResourceMemory:
				limitMem += rQuantity.Value()
			case apiv1.ResourceCPU:
				limitCpu += rQuantity.MilliValue()
			}
		}
	}
	return
}

func WaitNodeCreate(client *clientset.Clientset, nodename string, timeOut time.Duration, stop *bool) error {
	startTime := time.Now()
	for {
		_, err := client.CoreV1().Nodes().Get(nodename, meta_v1.GetOptions{})
		if err == nil {
			return nil
		}
		if stop != nil && *stop == true {
			return nil
		}
		if time.Since(startTime) > timeOut {
			return fmt.Errorf("timeout")
		}
		time.Sleep(300 * time.Millisecond)
	}
}

func WaitNodeReady(client *clientset.Clientset, nodename string, timeOut time.Duration, stop *bool) error {
	startTime := time.Now()
	for {
		node, err := client.CoreV1().Nodes().Get(nodename, meta_v1.GetOptions{})
		if err == nil && isNodeReady(node) == true {
			return nil
		}
		if stop != nil && *stop == true {
			return nil
		}
		if time.Since(startTime) > timeOut {
			return fmt.Errorf("timeout")
		}
		time.Sleep(100 * time.Millisecond)
	}
}

func GetClusterInf(client *clientset.Clientset) (ClusterInfo, error) {
	ret := ClusterInfo{}
	listNode, errNode := client.CoreV1().Nodes().List(meta_v1.ListOptions{})
	if errNode != nil {
		return ret, errNode
	}

	ret.TotalNode = int64(len(listNode.Items))
	for i := range listNode.Items {
		resCpu := listNode.Items[i].Status.Allocatable[apiv1.ResourceCPU]
		ret.TotalCpu += resCpu.MilliValue()

		resMem := listNode.Items[i].Status.Allocatable[apiv1.ResourceMemory]
		ret.TotalMem += resMem.Value()

		if isNodeReady(&listNode.Items[i]) {
			ret.ReadyNode++
		}
		if IsNodeLabel(&listNode.Items[i], provide_createnode_labelname, provide_createnode_labelvalue) {
			ret.ProviderNodeNum++
		}
	}

	listPod, errPod := client.CoreV1().Pods(apiv1.NamespaceAll).List(meta_v1.ListOptions{})
	if errPod != nil {
		return ret, errPod
	}
	ret.TotalPod = int64(len(listPod.Items))
	for i := range listPod.Items {
		if isPodReady(&listPod.Items[i]) {
			ret.ReadyPod++
		}
		reqCpu, reqMem, limitCpu, limitMem := getPodReqAndLimitRes(&listPod.Items[i])
		ret.LimitedCpu += limitCpu
		ret.LimitedMem += limitMem
		ret.RequestedCpu += reqCpu
		ret.RequestedMem += reqMem
	}
	ret.NotReadyNode = ret.TotalNode - ret.ReadyNode
	ret.NotReadyPod = ret.TotalPod - ret.ReadyPod
	if ret.TotalCpu > 0 {
		ret.RequestCpuRatio = float64(ret.RequestedCpu*100) / float64(ret.TotalCpu)
	} else {
		ret.RequestCpuRatio = 100.00
	}
	if ret.TotalCpu > 0 {
		ret.LimitCpuRatio = float64(ret.LimitedCpu*100) / float64(ret.TotalCpu)
	} else {
		ret.LimitCpuRatio = 100.00
	}

	if ret.TotalMem > 0 {
		ret.RequestMemRatio = float64(ret.RequestedMem*100) / float64(ret.TotalMem)
	} else {
		ret.RequestMemRatio = 100.00
	}
	if ret.TotalMem > 0 {
		ret.LimitMemRatio = float64(ret.LimitedMem*100) / float64(ret.TotalMem)
	} else {
		ret.LimitMemRatio = 100.00
	}
	return ret, nil
}

type CreatedNodeInfo struct {
	TotalCreatedNode int
	TotalCreatedCpu  int
	TotalCreatedMem  int // MB
	RunningNode      int
	RunningCpu       int
	RunningMem       int // MB
	MaxNodeIndex     int
}

type GetRetItem struct {
	Code int    `json:code`
	Msg  string `json:msg`
	Err  string `json:err`
	Data string `json:data`
}

func getNodeIndex(name string) int {
	strs := strings.Split(name, "-")
	if len(strs) == 2 {
		i, err := strconv.Atoi(strs[1])
		if err != nil {
			return -1
		}
		return i
	}
	return -1
}

func GetCreatedNodeInfo(url string) (CreatedNodeInfo, error) {
	ret := CreatedNodeInfo{}
	machines, err := GetCreatedMachine(url)
	if err != nil {
		return ret, err
	}
	for _, m := range machines {
		index := getNodeIndex(m.Name)
		if index > ret.MaxNodeIndex {
			ret.MaxNodeIndex = index
		}
		if m.Status == common.MachineRunning {
			ret.RunningCpu += m.CPU
			ret.RunningMem += m.Memory
			ret.RunningNode++
		}
		ret.TotalCreatedCpu += m.CPU
		ret.TotalCreatedMem += m.Memory
		ret.TotalCreatedNode++
	}
	return ret, nil
}

func LabelNode(client *clientset.Clientset, nodeName1, nodeName2, labelName, labeValue string) error {
	tryNum := 3
	var err error
	for i := 0; i < tryNum; i++ {
		newNode, errGet := client.CoreV1().Nodes().Get(nodeName1, meta_v1.GetOptions{})
		if errGet != nil && nodeName2 != "" {
			newNode, errGet = client.CoreV1().Nodes().Get(nodeName2, meta_v1.GetOptions{})
		}
		if errGet == nil && newNode != nil {
			if newNode.Labels == nil {
				newNode.Labels = make(map[string]string)
			}
			newNode.Labels[labelName] = labeValue
			_, errUpdate := client.CoreV1().Nodes().Update(newNode)
			if errUpdate == nil {
				return nil
			}
			err = errUpdate
		} else {
			err = errGet
		}
		time.Sleep(100 * time.Millisecond)
	}
	return err
}

func DeleteNode(client *clientset.Clientset, nodename1, nodename2 string) error {
	var err1, err2 error
	err1 = client.CoreV1().Nodes().Delete(nodename1, nil)
	if nodename2 != "" {
		err2 = client.CoreV1().Nodes().Delete(nodename2, nil)
	}
	if err1 != nil && (err2 != nil || nodename2 == "") {
		return err1
	}
	return nil
}
func IsNodeLabel(node *apiv1.Node, labelName, labeValue string) bool {
	if node == nil || node.Labels == nil {
		return false
	}
	if value, exist := node.Labels[labelName]; exist == true && value == labeValue {
		return true
	}
	return false
}
