package app

import (
	"fmt"
	"sort"
	"sync"
	"time"
	"yunprovider/log"

	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

type CreateStep int

const (
	CreateStepPre = iota
	CreateStepCreated
	CreateStepStarted
	CreateStepDeployed
	CreateStepJoined
	CreateStepReady
	CreateStepSuccess
)

type MachineCreateStoped struct {
	step string
}

func (e MachineCreateStoped) Error() string {
	return fmt.Sprintf("MachineCreateStoped is stoped at %s", e.step)
}

type MachineCreateFlowList []*MachineCreateFlow

func (mcfl MachineCreateFlowList) Less(i, j int) bool {
	return mcfl[i].curStep < mcfl[j].curStep
}
func (mcfl MachineCreateFlowList) Len() int {
	return len(mcfl)
}
func (mcfl MachineCreateFlowList) Swap(i, j int) {
	mcfl[i], mcfl[j] = mcfl[j], mcfl[i]
}

type MachineCreateFlow struct {
	curStep            CreateStep
	needDeploy         bool
	providerUrl        string
	deployrUrl         string
	machineName        string
	client             *clientset.Clientset
	hostName           string
	password           string
	joinClusterTimeOut time.Duration
	statusChan         chan error
	isStoped           bool
	existedChan        chan struct{}
}

func (mcf *MachineCreateFlow) Start() {
	log.MyLogI("MachineCreateFlow of %s started\n", mcf.machineName)
	defer log.MyLogI("MachineCreateFlow of %s exist\n", mcf.machineName)
	defer close(mcf.statusChan)
	defer close(mcf.existedChan)

	// create machine
	mcf.curStep = CreateStepPre
	machineCreate, errCreate := CreateMachine(mcf.providerUrl, mcf.machineName, mcf.hostName)
	if errCreate != nil {
		mcf.statusChan <- errCreate
		return
	}
	mcf.curStep = CreateStepCreated
	if mcf.isStoped == true {
		RecycleMachine(mcf.providerUrl, machineCreate, 0, 100, 1*time.Second)
		mcf.statusChan <- MachineCreateStoped{"created"}
		return
	}

	// start machine
	machine, errStart := StartMachine(mcf.providerUrl, mcf.machineName)
	if errStart != nil {
		RecycleMachine(mcf.providerUrl, machine, 0, 100, 1*time.Second)
		DeleteNode(mcf.client, machine.InnerIP, machine.HostName)
		mcf.statusChan <- errStart
		return
	}
	mcf.curStep = CreateStepStarted
	if mcf.isStoped == true {
		RecycleMachine(mcf.providerUrl, machineCreate, 0, 100, 1*time.Second)
		DeleteNode(mcf.client, machine.InnerIP, machine.HostName)
		mcf.statusChan <- MachineCreateStoped{"started"}
		return
	}

	// start deploy
	if mcf.needDeploy {
		errDeploy := DeployMachine(mcf.deployrUrl, machine.InnerIP, mcf.password)
		if errDeploy != nil {
			RecycleMachine(mcf.providerUrl, machine, 0, 100, 1*time.Second)
			DeleteNode(mcf.client, machine.InnerIP, machine.HostName)
			mcf.statusChan <- errDeploy
			return
		}
	}
	mcf.curStep = CreateStepDeployed
	if mcf.isStoped == true {
		RecycleMachine(mcf.providerUrl, machineCreate, 0, 100, 1*time.Second)
		DeleteNode(mcf.client, machine.InnerIP, machine.HostName)
		mcf.statusChan <- MachineCreateStoped{"deployed"}
		return
	}
	// wait machine join cluster
	errwait := WaitNodeCreate(mcf.client, machine.InnerIP, mcf.joinClusterTimeOut, &mcf.isStoped)
	if errwait != nil {
		RecycleMachine(mcf.providerUrl, machine, 0, 10, 3*time.Second)
		DeleteNode(mcf.client, machine.InnerIP, machine.HostName)
		mcf.statusChan <- fmt.Errorf("wait machine %s join cluster err:%v", machine.InnerIP, errwait)
		return
	}
	mcf.curStep = CreateStepJoined
	if mcf.isStoped == true {
		RecycleMachine(mcf.providerUrl, machineCreate, 0, 10, 3*time.Second)
		DeleteNode(mcf.client, machine.InnerIP, machine.HostName)
		mcf.statusChan <- MachineCreateStoped{"joined"}
		return
	}

	// wait node ready
	errReady := WaitNodeReady(mcf.client, machine.InnerIP, 30*time.Second, &mcf.isStoped)
	if errReady != nil {
		RecycleMachine(mcf.providerUrl, machine, 0, 10, 3*time.Second)
		DeleteNode(mcf.client, machine.InnerIP, machine.HostName)
		mcf.statusChan <- fmt.Errorf("wait machine %s ready err:%v", machine.InnerIP, errReady)
		return
	}
	mcf.curStep = CreateStepReady
	if mcf.isStoped == true {
		RecycleMachine(mcf.providerUrl, machineCreate, 0, 10, 3*time.Second)
		DeleteNode(mcf.client, machine.InnerIP, machine.HostName)
		mcf.statusChan <- MachineCreateStoped{"ready"}
		return
	}

	// set node label
	errLable := LabelNode(mcf.client, machine.InnerIP, "", provide_createnode_labelname, provide_createnode_labelvalue)
	if errLable != nil {
		RecycleMachine(mcf.providerUrl, machine, 0, 10, 3*time.Second)
		DeleteNode(mcf.client, machine.InnerIP, machine.HostName)
		mcf.statusChan <- errLable
		return
	}
	mcf.curStep = CreateStepSuccess
}

func (mcf *MachineCreateFlow) Stop() {
	mcf.isStoped = true
}

func (mcf *MachineCreateFlow) WaitStop() {
	mcf.isStoped = true
	<-mcf.existedChan
}

func (mcf *MachineCreateFlow) Status() string {
	if mcf.isStoped {
		return "Stopping"
	}
	switch mcf.curStep {
	case CreateStepPre:
		return "Prepare"
	case CreateStepCreated:
		return "Created"
	case CreateStepStarted:
		return "Started"
	case CreateStepDeployed:
		return "Deployed"
	case CreateStepJoined:
		return "Joined"
	case CreateStepReady:
		return "Ready"
	case CreateStepSuccess:
		return "Success"
	default:
		return "Unknow"
	}
}

type MachineManager struct {
	mu                 sync.Mutex
	machineMap         map[string]bool
	nextCreateIndex    int
	needDeploy         bool
	providerUrl        string
	deployrUrl         string
	client             *clientset.Clientset
	password           string
	joinClusterTimeOut time.Duration
	creatingFlow       []*MachineCreateFlow
}

func NewMachineManager(nextCreateIndex, maxCreateNum int, needDeploy bool, providerUrl, deployrUrl, password string, client *clientset.Clientset,
	joinClusterTimeOut time.Duration) *MachineManager {
	ret := &MachineManager{
		nextCreateIndex:    nextCreateIndex,
		needDeploy:         needDeploy,
		providerUrl:        providerUrl,
		deployrUrl:         deployrUrl,
		client:             client,
		password:           password,
		joinClusterTimeOut: joinClusterTimeOut,
		creatingFlow:       make([]*MachineCreateFlow, 0, maxCreateNum),
		machineMap:         make(map[string]bool),
	}
	return ret
}
func (mm *MachineManager) CreateMachine(num int) {
	mm.mu.Lock()
	defer mm.mu.Unlock()
	diff := num - len(mm.creatingFlow)
	if diff > 0 {
		for i := 0; i < diff; i++ {
			machineName, hostname := getMachineNameAndHostnameByIndex(mm.nextCreateIndex)
			mm.nextCreateIndex++
			mcf := &MachineCreateFlow{
				needDeploy:         mm.needDeploy,
				providerUrl:        mm.providerUrl,
				deployrUrl:         mm.deployrUrl,
				machineName:        machineName,
				client:             mm.client,
				hostName:           hostname,
				password:           mm.password,
				joinClusterTimeOut: mm.joinClusterTimeOut,
				statusChan:         make(chan error, 0),
				existedChan:        make(chan struct{}, 0),
			}
			go mcf.Start()
			mm.machineMap[machineName] = true
			mm.creatingFlow = append(mm.creatingFlow, mcf)
		}
	} else {
		sort.Sort(MachineCreateFlowList(mm.creatingFlow))
		for i := len(mm.creatingFlow) - 1; i >= 0; i-- {
			if diff >= 0 {
				break
			}
			mcf := mm.creatingFlow[i]
			if mcf.curStep >= CreateStepSuccess {
				continue
			}
			mcf.Stop()
			diff++
		}
	}
}

func (mm *MachineManager) IsMachineCreating(machinename string) bool {
	mm.mu.Lock()
	defer mm.mu.Unlock()
	_, exist := mm.machineMap[machinename]
	return exist
}
func (mm *MachineManager) Check() {
	mm.mu.Lock()
	defer mm.mu.Unlock()
	saveCreatingFlow := make([]*MachineCreateFlow, 0, len(mm.creatingFlow))
	for _, mcf := range mm.creatingFlow {
		select {
		case err := <-mcf.statusChan:
			if err != nil {
				log.MyLogE("machine %s create err exit:%v", mcf.machineName, err)
			} else {
				log.MyLogI("machine %s create success", mcf.machineName)
			}
			delete(mm.machineMap, mcf.machineName)
		default:
			log.MyLogI("machine %s is at create step %s", mcf.machineName, mcf.Status())
			saveCreatingFlow = append(saveCreatingFlow, mcf)
		}
	}
	mm.creatingFlow = saveCreatingFlow
}
