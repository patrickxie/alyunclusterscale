package main

import (
	"fmt"
	"os"
	"yunprovider/cmd/clustermanager/app"
	"yunprovider/cmd/clustermanager/app/options"
	"yunprovider/version"

	"github.com/golang/glog"
	"github.com/spf13/pflag"
)

func main() {
	clusterManager := options.NewClusterManagerConfig()
	clusterManager.AddFlags(pflag.CommandLine)

	options.InitFlags()
	defer glog.Flush()

	version.PrintAndExitIfRequested()
	if err := app.Run(clusterManager); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}
