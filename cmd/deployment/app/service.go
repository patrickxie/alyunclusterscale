package app

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os/exec"
	"path"
	"strings"
	"yunprovider/cmd/deployment/app/options"

	"github.com/emicklei/go-restful"
	"github.com/golang/glog"
)

func Run(s *options.DeploymentConfig) error {
	if err := run(s); err != nil {
		return fmt.Errorf("failed to run deployment : %v", err)

	}
	return nil
}

func checkConfig(s *options.DeploymentConfig) error {
	if s.HttpServerAddr == "" {
		return fmt.Errorf("httpserveraddr is empty")
	}
	if s.DeployScriptPath == "" {
		return fmt.Errorf("deployscriptpath is empty")
	}
	return nil
}

func run(s *options.DeploymentConfig) error {
	errCheck := checkConfig(s)
	if errCheck != nil {
		return errCheck
	}
	installHttpServer(s.HttpServerAddr, s.DeployScriptPath)
	return nil
}

var wsContainer *restful.Container = restful.NewContainer()

func Health(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("OK"))
}

func installHttpServer(addr, scriptPath string) {
	mux := http.NewServeMux()
	mux.HandleFunc("/health", Health)
	wsContainer.Router(restful.CurlyRouter{})
	wsContainer.ServeMux = mux
	httpServer := &httpServer{scriptPath: scriptPath}

	// deploy machine
	ws1 := new(restful.WebService)
	ws1.Path("/deploy").Consumes("*/*").Produces(restful.MIME_JSON)
	ws1.Route(ws1.POST("/{machine-ip}").To(httpServer.DeployMachine).
		Doc("deploy a new machine").
		Param(ws1.PathParameter("machine-ip", "the ip of the machine to deploy").DataType("string")))
	ws1.Route(ws1.GET("/{machine-ip}/{machine-password}").To(httpServer.DeployMachine).
		Doc("deploy a new machine").
		Param(ws1.PathParameter("machine-ip", "the ip of the machine to deploy").DataType("string")).
		Param(ws1.PathParameter("machine-password", "the password of the machine to deploy").DataType("string")))

	wsContainer.Add(ws1)
	server := &http.Server{Addr: addr, Handler: wsContainer}
	glog.Fatal(server.ListenAndServe())
}

type httpServer struct {
	scriptPath string
}

func (hs *httpServer) DeployMachine(request *restful.Request, response *restful.Response) {
	machineip := request.PathParameter("machine-ip")
	machinepassword := request.PathParameter("machine-password")

	fmt.Printf("start deploy machineip=%s, machinepassword=%s\n", machineip, machinepassword)

	var stdout bytes.Buffer
	var stderr bytes.Buffer

	cmd := exec.Command("/bin/bash", hs.scriptPath, machineip, machinepassword)
	cmd.Dir = path.Dir(hs.scriptPath)
	cmd.Stdout, cmd.Stderr = &stdout, &stderr

	if err := cmd.Start(); err != nil {
		writeResponse(response, err, "start deploy ok", "start deploy fail", nil)
		fmt.Printf("failed to exec cmd %v - %v; stderr: %v", cmd.Args, err, stderr.String())
		return
	}
	if err := cmd.Wait(); err != nil {
		writeResponse(response, err, "wait deploy ok", "wait deploy fail", nil)
		fmt.Printf("cmd %v failed. stderr: %s; err: %v", cmd.Args, stderr.String(), err)
		return
	}
	writeResponse(response, nil, "wait deploy ok", "wait deploy fail", nil)
	fmt.Printf("deploy machineip=%s success\n", machineip)
}

func writeResponse(w io.Writer, err error, msgOk, msgFail string, object interface{}) {
	var retCode int
	var retErrStr string
	var objStr string
	var msg string
	if err != nil {
		retCode = 201
		retErrStr = err.Error()
		msg = msgFail
	} else {
		retCode = 200
		retErrStr = ""
		msg = msgOk
	}
	if object != nil && err == nil {
		buf, _ := json.MarshalIndent(object, " ", "  ")
		objStr = string(buf)

	}

	tmp := fmt.Sprintf("{\"code\":%d,\"msg\":\"%s\",\"err\":\"%s\", \"data\":\"%s\"}",
		retCode, msg, retErrStr, strings.Replace(objStr, "\"", "\\\"", -1))
	w.Write([]byte(tmp))
}
