package options

import (
	"github.com/spf13/pflag"
)

type DeploymentConfig struct {
	HttpServerAddr   string
	DeployScriptPath string
}

func (dc *DeploymentConfig) AddFlags(fs *pflag.FlagSet) {
	fs.StringVar(&dc.HttpServerAddr, "httpserveraddr", dc.HttpServerAddr,
		"Http server addr.")
	fs.StringVar(&dc.DeployScriptPath, "deployscriptpath", dc.DeployScriptPath,
		"Path of deployment script.")
}

func NewDeploymentConfig() *DeploymentConfig {
	return &DeploymentConfig{
		HttpServerAddr: ":8086",
	}
}

func InitFlags() {
	pflag.Parse()

}
